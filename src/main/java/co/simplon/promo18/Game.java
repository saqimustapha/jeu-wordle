package co.simplon.promo18;

import java.util.Scanner;

public class Game {
  public static Scanner sc = new Scanner(System.in);
  public static final String ANSI_RESET = "\u001B[0m";
  public static final String ANSI_YELLOW = "\u001B[33m";
  public static final String ANSI_GREEN = "\u001B[32m";

  public void startGame() {
    System.out.println("Deviner un mot à cinq lettres");
    String answerChoosen = ReturnRandomWord();
    char[] answer = new char[5];
    for (int i = 0; i < 5; i++)
      answer[i] = answerChoosen.charAt(i);
    char[] input = new char[5];
    boolean done = false;
    

    while (!done) {
      
      String response = sc.nextLine().toLowerCase();
      while (response.length() < 5) {
        response = sc.nextLine().toLowerCase();
      }
      for (int i = 0; i < 5; i++) { 
        answer[i] = answerChoosen.charAt(i);
        input[i] = response.charAt(i);
      }
      if (PrintWordWithColor(input, answer))
        done = true;
    }
    System.out.println("Bravo, vous avez touvé la réponse");
  }
  public static boolean PrintWordWithColor(char[] inputWord, char[] correctWord) {
    boolean correct = true;
    char[] answerTemp = correctWord;
    int[] colorForLetter = new int[5]; 

    for (int i = 0; i < 5; i++) { 

      if (inputWord[i] == answerTemp[i]) {
        answerTemp[i] = '-';
        colorForLetter[i] = 2;
      } else
        correct = false;
    }

    for (int j = 0; j < 5; j++) { 
      for (int k = 0; k < 5; k++) {
        if (inputWord[j] == answerTemp[k] && colorForLetter[j] != 2) {
          
          colorForLetter[j] = 1;
          answerTemp[k] = '-';
        }
      }
    }

    for (int m = 0; m < 5; m++) {
      if (colorForLetter[m] == 0)
        System.out.print(inputWord[m]);
      if (colorForLetter[m] == 1)
        System.out.print(ANSI_YELLOW + inputWord[m] + ANSI_RESET);
      if (colorForLetter[m] == 2)
        System.out.print(ANSI_GREEN + inputWord[m] + ANSI_RESET);
    }
    System.out.println("");
    return correct;
  }

  public static String ReturnRandomWord() {
    String[] answerList = {"chien", "torse", "fille", "pause", "masse", "pomme", "femme", "homme",
  "cages"}; // exemple de mots à cinq caractères
    return answerList[(int) (Math.random() * (answerList.length - 1))];
  }
}
