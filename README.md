**Le WORDLE :**

Les règles du jeu : proposez des mots de 5 lettres. 
Les bonnes lettres au bon endroit seront en vert.
Les bonnes lettres aux mauvais endroits seront en jaunes.
Les mauvaises en grises. 

Vous avez 5 à 6 essais pour trouver le bon mot.


Le mot à deviner est généré aléatoirement par une méthode random.

Pour trouver le mot à deviner : il faut comparer entre les index du mot tapé dans la console et le mot à deviner choisi au hasard dans la liste random, et faire ensuite des conditions pour vérifier :

Si toute position est correcte, on l'imprimera en vert. 
S'il y a une lettre correcte (jaune) qui n'est pas déjà verte et correspondant à une autre lettre.
Sinon la lettre est fausse et elle aura la couleur grise.

Pistes d'amélioration :

On peut mettre une large liste de mots de cinq lettre dans un fichier pour enrichir le jeu.

On peut améliorer le jeu en ajoutant une fonction temps en seconde qui va compter le temps écoulé pour qu'un utilisateur trouve un mot par exemple.

